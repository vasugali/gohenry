package com.gohenry.test.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.gohenry.test.dao.ParentDAO;
import com.gohenry.test.domain.Children;
import com.gohenry.test.domain.Parent;


@RunWith(SpringRunner.class)
@WebMvcTest(ParentsController.class)
public class ParentsControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ParentDAO parentsDAO;
	

	List<Parent> parentsList = new ArrayList<Parent>();

	Map<String, Parent> parentMap = new HashMap<String, Parent>();

	@Test
	public void getParentsTest() throws Exception {
		when(parentsDAO.getAllParents()).thenReturn(getParentsList());

		mvc.perform(get("/parents").accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].firstName", is("FirstName:0")))
				.andExpect(jsonPath("$[3].lastName", is("LastName:3")));
	}

	@Test
	public void personNotFoundTest() throws Exception {
		mvc.perform(get("/parents/7").accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound());
	}

	@Before
	public void createParentsData() {
		for (int i = 0; i < 5; i++) {
			Parent p = new Parent();
			p.setId(i);
			p.setTitle("Mr" + i);
			p.setFirstName("FirstName:" + i);
			p.setLastName("LastName:" + i);
			p.setDateOfBirth("DOB:" + i);
			p.setEmailAddress("a@abc.com" + i);
			p.setGender("Male" + i);

			Set<Children> childrenList = new HashSet<Children>();
			for (int j = 0; j <= i; j++) {
				Children c = new Children();
				c.setId(j + 1);
				c.setFirstName("CFirstName:" + j);
				c.setDateOfBirth("CdateOfBirth" + j);
				c.setEmailAddress("CemailAddress:" + j);
				c.setGender("CMale" + j);
				c.setLastName("ClastName:" + j);
				childrenList.add(c);
			}
			p.setChildrenList(childrenList);
			parentMap.put("" + p.getId(), p);
			parentsList.add(p);
		}

	}

	/**
	 * @return the parentsList
	 */
	public List<Parent> getParentsList() {
		return parentsList;
	}

	/**
	 * @param parentsList the parentsList to set
	 */
	public void setParentsList(List<Parent> parentsList) {
		this.parentsList = parentsList;
	}

	/**
	 * @return the parentMap
	 */
	public Map<String, Parent> getParentMap() {
		return parentMap;
	}

	/**
	 * @param parentMap the parentMap to set
	 */
	public void setParentMap(Map<String, Parent> parentMap) {
		this.parentMap = parentMap;
	}
	
	

}