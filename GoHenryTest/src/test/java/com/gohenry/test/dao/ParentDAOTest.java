/**
 * 
 */
package com.gohenry.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author T460
 *
 */
@RunWith(SpringRunner.class)
public class ParentDAOTest {
	ParentDAO parentDAO;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		parentDAO= new ParentDAO();
	}

	
	/**
	 * Test method for {@link com.gohenry.test.dao.ParentDAO#getAllParents()}.
	 */
	@Test
	public void testGetAllParents() {
		assertEquals(5,parentDAO.getAllParents().size());
	}

	/**
	 * Test method for {@link com.gohenry.test.dao.ParentDAO#getParent(java.lang.String)}.
	 */
	@Test
	public void testGetParent() {
		assertEquals("FirstName:1", parentDAO.getParent("1").getFirstName());
	}

	
	@Test
	public void testGetParentNotFound() {
		assertNull(parentDAO.getParent("8"));
	}

	@Test
	public void testGetParentAndChildren() {
		assertEquals(1, parentDAO.getParent("0").getChildrenList().size());
	}
}
