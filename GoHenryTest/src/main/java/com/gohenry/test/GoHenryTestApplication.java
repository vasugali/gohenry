package com.gohenry.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoHenryTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoHenryTestApplication.class, args);
	}
}
