package com.gohenry.test.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gohenry.test.dao.ParentDAO;
import com.gohenry.test.domain.Parent;

@RestController
public class ParentsController {

	@Autowired
	private ParentDAO parentDAO;

	/**
	 * This Api will return the all the parents
	 * 
	 * @return
	 */
	@RequestMapping(value = "/parents", 
			method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public List<Parent> getParents() {
		List<Parent> list = parentDAO.getAllParents();
		return list;
	}

	/**
	 * This one returns the parent based on id.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/parents/{id}",
			method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity<?> getParent(@PathVariable("id") String id) {

		Parent parent = parentDAO.getParent(id);
		if (parent != null) {
			ResponseEntity<?> re = new ResponseEntity<>((Object) parent, HttpStatus.FOUND);
			return re;
		} else {
			ResponseEntity<?> re = new ResponseEntity<>((Object) null, HttpStatus.NOT_FOUND);
			return re;
		}
	}

}