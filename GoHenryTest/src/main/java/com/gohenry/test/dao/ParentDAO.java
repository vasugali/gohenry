package com.gohenry.test.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.gohenry.test.domain.Children;
import com.gohenry.test.domain.Parent;

/**
 * 
 * @author T460
 *
 */
@Repository
public class ParentDAO {

	public ParentDAO() {
	};
	
	/**
	 * This DAO method returns all the parents with children
	 * @return
	 */

	public List<Parent> getAllParents() {
		createParentsData();
		return parentsList;
	}

	/**
	 * This method returns the parent with the children associated with ID.
	 * @param id
	 * @return
	 */
	public Parent getParent(String id) {
		createParentsData();
		return (parentMap.get(id) != null) ? parentMap.get(id) : null;
	}

	public List<Parent> getParentsList() {
		return parentsList;
	}

	public void setParentsList(List<Parent> parentsList) {
		this.parentsList = parentsList;
	}

	public Map<String, Parent> getParentMap() {
		return parentMap;
	}

	public void setParentMap(Map<String, Parent> parentMap) {
		this.parentMap = parentMap;
	}

	List<Parent> parentsList = new ArrayList<Parent>();

	Map<String, Parent> parentMap = new HashMap<String, Parent>();

	public void createParentsData() {
		for (int i = 0; i < 5; i++) {
			Parent p = new Parent();
			p.setId(i);
			p.setTitle("Mr" + i);
			p.setFirstName("FirstName:" + i);
			p.setLastName("LastName:" + i);
			p.setDateOfBirth("DOB:" + i);
			p.setEmailAddress("a@abc.com" + i);
			p.setGender("Male" + i);

			Set<Children> childrenList = new HashSet<Children>();
			for (int j = 0; j <= i; j++) {
				Children c = new Children();
				c.setId(j + 1);
				c.setFirstName("CFirstName:" + j);
				c.setDateOfBirth("CdateOfBirth" + j);
				c.setEmailAddress("CemailAddress:" + j);
				c.setGender("CMale" + j);
				c.setLastName("ClastName:" + j);
				childrenList.add(c);
			}
			p.setChildrenList(childrenList);
			parentMap.put("" + p.getId(), p);
			parentsList.add(p);
		}
	}

}
